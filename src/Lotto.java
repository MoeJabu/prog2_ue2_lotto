
/*Lotto
 * draws an amount of lottotips, depending und current time.
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 13.3.2021
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Lotto {

	public static ArrayList<Integer> getSingleDigits(long input) {
		long sum = 0;
		ArrayList<Integer> singleDigits = new ArrayList<Integer>();

		while (input != 0) {
			long digit = input % 10;
			singleDigits.add((int) digit);
			input /= 10;
		}
		return singleDigits;
	}

	public static int crossSum(long input) {
		long sum = 0;

		while (input != 0) {
			sum += input % 10;
			input /= 10;
		}
		return (int) sum;
	}

	public static void main(String[] args) {
		// settings
		Locale.setDefault(new java.util.Locale("en", "US"));

		// vars
		long curentTime = System.currentTimeMillis();

		// Prog
//		System.out.println("Current time in ms: "+curentTime);
//		System.out.println("Single digits of current time: "+getSingleDigits(curentTime));
//		System.out.println("Sum of all digits: "+crossSum(curentTime));

		for (int i = 1; i <= crossSum(curentTime); i++) {
			System.out.printf("Tipp %d: " + LotteryDrawing.drawASetOfNumbers()+"\n",i);
		}

	}

}
