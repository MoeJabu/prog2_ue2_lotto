/*Lotto
 * LotteryDraw - provides lotto drawing functionality
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 13.3.2021
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class LotteryDrawing {
	final static int SIZE_OF_NUMBERPOOL = 45;
	final static int AMOUNT_OF_DRAWINGS = 6;

	public static int drawALottoNumber() {
		Random ran = new Random();
		int number = 0;

		do {
			number = ran.nextInt(SIZE_OF_NUMBERPOOL);
		} while (number <= 0 && number > SIZE_OF_NUMBERPOOL);

		return number;
	}

	public static ArrayList<Integer> drawASetOfNumbers() {
		ArrayList<Integer> setOfNumbers = new ArrayList<Integer>();

		for (int i = 1; i <= AMOUNT_OF_DRAWINGS; i++) {
			int number = drawALottoNumber();
			if (!setOfNumbers.contains(number)) {
				setOfNumbers.add(number);
			} else {
				i--;
			}
		}

		Collections.sort(setOfNumbers);
		return setOfNumbers;
	}
}
